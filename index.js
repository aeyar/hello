let first = document.querySelector('.first')

let logo = document.querySelector('#logo')

let sidebar = document.querySelector('.sidebar')
let check = document.querySelector('#check')

// sidebar.style.left = '-300px';

let linkPortfolio = document.querySelector('.link-Portfolio')
let linkAbout = document.querySelector('.link-About')
let linkContact = document.querySelector('.link-Contact')

let view = document.querySelector('.view')

first.style.opacity = 1;

linkPortfolio.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class="works">
          <h2>PORTFOLIO</h2>
          <div class='portfolio-cards'>
            <li class="x">
              <h3>STATIC WEBSITE</h3>
              <a href="https://aeyar.gitlab.io/portfolio-v2/" target='_blank'>View</a>
            </li>
            <li class="y">
              <h3>BOOKING SERVICES</h3>
              <a href="https://aeyar.gitlab.io/first-heroku/" target='_blank'>View</a>
            </li>
            <li class="z">
              <h3>BUDGET TRACKING APP</h3>
              <a href="https://money-mo-bud.vercel.app/" target='_blank'>View</a>
            </li>
          </div>
        </div>`
	},500)

})

linkContact.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class='contact'>
          <div class='contact-image'>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.1384156357103!2d120.99212181406091!3d14.704763389736232!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b15bcc3a8ce3%3A0xdc7c4b8ef825fa9e!2s232%20P.%20Santiago%20St%2C%20Manila%2C%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1606994567819!5m2!1sen!2sph" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          </div>
          <div class='contact-details'>
              <h1>GET IN TOUCH</h1>
              <br>
              <br>
              <p>Address: 232 P. Santiago Street, Paso de Blas, Valenzuela City</p>
              <br>
              <br>
              <p>Mobile: 0967-417-5347  |  Email: allanrobinbueno@gmail.com</p>
              <br>
              <br>
              <form action="mailto:allanrobinbueno@gmail.com" method="post" enctype="text/plain">
              First Name*<br>
              <input type="text" name="fname" required><br>
              Last Name*<br>
              <input type="text" name="lname" required=""><br>
              E-mail*<br>
              <input type="text" name="mail" required=""><br>
             
              Message*<br>
              <textarea name="comment" rows='4'>Please Leave a Message...</textarea><br><br>
              <input type="submit" value="Send">
              <input type="reset" value="Reset">
              </form>

          </div>
        </div>`
	},500)

})

logo.addEventListener('click', e=>{
	e.preventDefault();
	location.reload();
})	

view.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class="works">
          <h2>PORTFOLIO</h2>
          <div class='portfolio-cards'>
            <li class="x">
              <h3>STATIC WEBSITE</h3>
              <a href="https://aeyar.gitlab.io/portfolio-v2/" target='_blank'>View</a>
            </li>
            <li class="y">
              <h3>BOOKING SERVICES</h3>
              <a href="https://aeyar.gitlab.io/first-heroku/" target='_blank'>View</a>
            </li>
            <li class="z">
              <h3>BUDGET TRACKING APP</h3>
              <a href="https://money-mo-bud.vercel.app/" target='_blank'>View</a>
            </li>
          </div>
        </div>`
	},500)

})

linkAbout.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class='about'>
          <div class='about-image'>
            <img src="images/me.png">
          </div>
          <div class='about-details'>
              <h1>ABOUT ME</h1>
              <br>
              <br>
              <p>Hello, <strong>I'm Allan Robin L. Bueno</strong>, a career shifter and now,</p>
              <p>a <strong>Full Stack Web Developer</strong>.</p>
              <br>
              <p>I'm a graduate of Bachelor of Science in Business Administration.</p>
              <p> I've been an admin officer for 9 years at a security agency.</p><p> One of my task there is to make applications which until now have been using by the company.</p><p> I was able to create it thru the help of cross-platform relational database application.</p><p> Because of that, I decided to pursue this career path, which originaly a childhood dream of mine.</p>
              <br>
              <p>I enrolled in full stack software engineering course at <strong>Zuitt Coding Bootcamp</strong>. </p><p>It's been tough and challenging but at the same time it is very fulfilling especially when you are able to make your own application. I was able to graduate having a second highest grade in our batch.</p>

              <br>

          </div>
        </div>`
	},500)
})



let linkPortfolio2 = document.querySelector('.link-Portfolio2')
let linkAbout2 = document.querySelector('.link-About2')
let linkContact2 = document.querySelector('.link-Contact2')


linkPortfolio2.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	check.checked = false;
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class="works">
          <h2>PORTFOLIO</h2>
          <div class='portfolio-cards'>
            <li class="x">
              <h3>STATIC WEBSITE</h3>
              <a href="https://aeyar.gitlab.io/portfolio-v2/" target='_blank'>View</a>
            </li>
            <li class="y">
              <h3>BOOKING SERVICES</h3>
              <a href="https://aeyar.gitlab.io/first-heroku/" target='_blank'>View</a>
            </li>
            <li class="z">
              <h3>BUDGET TRACKING APP</h3>
              <a href="https://money-mo-bud.vercel.app/" target='_blank'>View</a>
            </li>
          </div>
        </div>`
	},500)

})

linkContact2.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	check.checked = false;

	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class='contact'>
          <div class='contact-image'>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.1384156357103!2d120.99212181406091!3d14.704763389736232!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b15bcc3a8ce3%3A0xdc7c4b8ef825fa9e!2s232%20P.%20Santiago%20St%2C%20Manila%2C%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1606994567819!5m2!1sen!2sph" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          </div>
          <div class='contact-details'>
              <h1>GET IN TOUCH</h1>
              <br>
            
              <p>Address: 232 P. Santiago Street, Paso de Blas, Valenzuela City</p>
              <br>
       
              <p>Mobile: 0967-417-5347
              </p>
              <br>
              <p>Email: allanrobinbueno@gmail.com</p>
              <br>
             
              <form action="mailto:allanrobinbueno@gmail.com" method="post" enctype="text/plain">
              First Name*<br>
              <input type="text" name="fname" required><br>
              Last Name*<br>
              <input type="text" name="lname" required=""><br>
              E-mail*<br>
              <input type="text" name="mail" required=""><br>
             
              Message*<br>
              <textarea name="comment" rows='4'>Please Leave a Message...</textarea><br>
              <input type="submit" value="Send">
              <input type="reset" value="Reset">
              </form>

          </div>
        </div>`
	},500)

})





linkAbout2.addEventListener('click', e=>{
	e.preventDefault();
	first.style.opacity = 0;
	
	check.checked = false;
	
	setTimeout(()=>{
		first.style.opacity = 1;

		first.innerHTML = `<div class='about'>
          <div class='about-image'>
            <img src="images/me.png">
          </div>
          <div class='about-details'>
              <h1>ABOUT ME</h1>
              <br>
              <p>Hello, <strong>I'm Allan Robin L. Bueno</strong>, a career shifter and now,</p>
              <p>a <strong>Full Stack Web Developer</strong>.</p>
              <br>
              <p>I'm a graduate of Bachelor of Science in Business Administration.</p>
              <p> I've been an admin officer for 9 years at a security agency.</p><p> One of my task there is to make applications which until now have been using by the company.</p><p> I was able to create it thru the help of cross-platform relational database application.</p><p> Because of that, I decided to pursue this career path, which originaly a childhood dream of mine.</p>
              <br>
              <p>I enrolled in full stack software engineering course at <strong>Zuitt Coding Bootcamp</strong>. </p><p>It's been tough and challenging but at the same time it is very fulfilling especially when you are able to make your own application.</p> <p>I was able to graduate having a second highest grade in our batch.</p>
              <br>

          </div>
        </div>`
	},500)
})